package bowling;

public class Game {

    private int countRolls;
    private int[] rolls = new int[21];

    public int score() {
        int score = 0;
        int firstInFrame = 0;
        for (int frame = 0; frame < 10; frame++) {
            if (isStrike(firstInFrame)) {
                score += 10 + strikeBonus(firstInFrame);
                firstInFrame += 1;
            } else if (isSpare(firstInFrame)) {
                score += 10 + spareBonus(firstInFrame);
                firstInFrame += 2;
            } else {
                score += simpleFrameScore(firstInFrame);
                firstInFrame += 2;
            }
        }

        return score;
    }

    private int strikeBonus(int firstInFrame) {
        return rolls[firstInFrame + 1] + rolls[firstInFrame + 2];
    }

    private boolean isStrike(int firstInFrame) {
        return rolls[firstInFrame] == 10;
    }

    private int simpleFrameScore(int firstInFrame) {
        return rolls[firstInFrame] + rolls[firstInFrame + 1];
    }

    private int spareBonus(int i) {
        return rolls[i + 2];
    }

    private boolean isSpare(int i) {
        return rolls[i] + rolls[i + 1] == 10;
    }

    public void roll(int pins) {
        rolls[countRolls] = pins;
        countRolls++;
    }
}
