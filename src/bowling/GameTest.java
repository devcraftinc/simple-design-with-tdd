package bowling;

import org.junit.Assert;
import org.junit.Test;

public class GameTest {

    Game g = new Game();

    @Test
    public void emptyGameShouldScoreZero(){
        Assert.assertEquals(0, g.score());
    }

    @Test
    public void simpleFrameShouldScoreNumOfFallingPins(){
        rollSimpleFrame(1, 2);
        Assert.assertEquals(3, g.score());
    }

    @Test
    public void shouldAddNextRollAsBonusOnSpare(){
        rollSpare();
        rollSimpleFrame(1, 2);
        Assert.assertEquals(14, g.score());
    }

    @Test
    public void shouldAddNext2RollsAsBonusOnStrike(){
        rollStrike();
        rollSimpleFrame(1, 2);
        Assert.assertEquals(16, g.score());
    }

    private void rollStrike() {
        g.roll(10);
    }

    private void rollSpare() {
        g.roll(1);
        g.roll(9);
    }

    private void rollSimpleFrame(int first, int second) {
        g.roll(first);
        g.roll(second);
    }

}
